<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->delete();

        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Agricultural Insurance‎',
                'slug' => str_slug('Agricultural insurance‎ '),
                'description' => 'Agricultural insurance‎ ',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Deposit insurance‎',
                'slug' => str_slug('Deposit insurance‎'),
                'description' => 'Deposit insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Flood insurance‎ ',
                'slug' => str_slug('Flood insurance‎ '),
                'description' => 'Flood insurance‎ ',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Health insurance‎',
                'slug' => str_slug('Health insurance‎'),
                'description' => 'Health insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Liability insurance‎',
                'slug' => str_slug('Liability insurance‎'),
                'description' => 'Liability insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Life insurance‎',
                'slug' => str_slug('Life insurance‎'),
                'description' => 'Life insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            6 => 
            array (
                'id' => 7,
                'title' => 'Mortgage insurance‎',
                'slug' => str_slug('Mortgage insurance‎'),
                'description' => 'Mortgage insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            7 => 
            array (
                'id' => 8,
                'title' => 'Property insurance‎',
                'slug' => str_slug('Property insurance‎'),
                'description' => 'Property insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            8 => 
            array (
                'id' => 9,
                'title' => 'Reinsurance‎',
                'slug' => str_slug('Reinsurance‎'),
                'description' => 'Reinsurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            9 => 
            array (
                'id' => 10,
                'title' => 'Self insurance‎',
                'slug' => str_slug('Self insurance‎'),
                'description' => 'Self insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            10 => 
            array (
                'id' => 11,
                'title' => 'Travel insurance‎',
                'slug' => str_slug('Travel insurance‎'),
                'description' => 'Travel insurance‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
            11 => 
            array (
                'id' => 12,
                'title' => 'Workers\' compensation‎',
                'slug' => str_slug('Workers\' compensation‎'),
                'description' => 'Workers\' compensation‎',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
                
            ),
           ));
    }
}
