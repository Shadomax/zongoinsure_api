<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('username', 255)->unique();
            $table->string('email', 255)->unique();
            $table->string('password');
            $table->string('url')->nullable();
            $table->boolean('is_completed')->default(0);
            $table->boolean('online')->default(0);
            $table->boolean('is_activated')->default(0);
            $table->string('last_login', 255)->nullable();
            $table->enum('account_type', ['company', 'user'])->default('user');
            $table->text('verification_code')->nullable();
            $table->string('avatar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::enableForeignKeyConstraints();
    }
}
