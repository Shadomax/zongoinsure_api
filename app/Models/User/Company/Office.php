<?php

namespace App\Models\User\Company;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'country_id', 'city_id', 'state_id', 'street', 'email', 'phone_number', 'longitude', 'latitude'
    ];

    public function company()
    {
    	return $this->belongsTo('App\Models\User\Company');
    }
}
