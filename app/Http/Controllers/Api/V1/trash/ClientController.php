<?php

namespace App\Http\Controllers\Api\V1;

use Storage;
use Carbon\Carbon;
use App\Models\Client;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('checkClient', ['except' => ['register', 'activate', 'verify', 'login', 'forgot_password', 'check_reset_password_token', 'reset_password']]);
        $this->middleware('auth:api', ['except' => []]);
    }

    /**
     * Update.
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            throw new \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException(null, 'You are not allowed to access the resource');
        }

        $this->validate($request, [
            'date_of_birth' => 'bail|required|string',
            'place_of_birth' => 'bail|required|string',
            'gender' => 'bail|required|string',
        ]);

        $updateClient = Client::where('user_id', $user->id)->update([
            'date_of_birth' => $request->date_of_birth,
            'place_of_birth' => $request->place_of_birth,
            'gender' => $request->gender,
        ]);

        if (!$updateClient) {
            throw new \Symfony\Component\HttpKernel\Exception\ConflictHttpException('Operation did not complete');
        }

        return $this->success('Updated account details');
    }
}
